﻿using System;
namespace sambocapp_api.Models.BO.Req
{
	public class UserChangePasswordReq
	{
        public int userId { get; set; }
        public string? currentPassword { get; set; }
        public string? newPassword { get; set; } 
    }
}

