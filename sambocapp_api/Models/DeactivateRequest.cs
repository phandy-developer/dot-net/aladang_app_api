namespace sambocapp_api.Models;

public class DeactivateRequest
{
    public int Id { get; set; }
    public string UserType { get; set; }
}