﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sambocapp_api.Models
{
	[Table("tbl_item")]
	public class Item
	{
		[Key]
		public int id { get; set; }
		public string? name { get; set; }
	}
}

