﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sambocapp_api.Models
{
    [Table("ReportTypes")]
	public class ReportType
	{
        [Key]
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}

