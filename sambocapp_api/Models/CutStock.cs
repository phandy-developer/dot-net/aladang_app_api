﻿namespace sambocapp_api.Models
{
    public class CutStock
    {
        public int? shopid { get; set; }
        public int? productid { get; set; }
        public int? qty { get; set; }
    }
}
