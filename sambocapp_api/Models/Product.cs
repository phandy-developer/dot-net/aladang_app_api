﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using sambocapp_api.Models.BO.Req;

namespace sambocapp_api.Models
{
	[Table("Products")]
	public class Product
	{
		[Key]
		public int id { get; set; }
		public int? ShopId { get; set; }
		public string? ProductCode { get; set; }
		public string? ProductName { get; set; }
		public string? Description { get; set; }
		public int? QtyInStock { get; set; }
		public decimal? Price { get; set; }
		public decimal? NewPrice { get; set; }
        public int? CurrencyId { get; set; }
		public string? CutStockType { get; set; }
		public DateTime? ExpiredDate { get; set; }
		public string? LinkVideo { get; set; }
		public string? ImageThumbnail { get; set; }
		public string? Status { get; set; }

		public void SetProductCreate(ProductReq data)
		{
			id = data.Id;
			ShopId = data.ShopId;
			ProductCode = data.ProductCode;
			ProductName = data.ProductName;
			Description = data.Description;
			QtyInStock = data.QtyInStock;
			Price = data.Price;
			NewPrice = data.NewPrice;
			CurrencyId = data.CurrencyId;
			CutStockType = data.CutStockType;
			ExpiredDate = data.ExpiredDate;
			LinkVideo = data.LinkVideo;
			ImageThumbnail = data.ImageThumbnail;
			Status = data.Status;
		}
	}
}

