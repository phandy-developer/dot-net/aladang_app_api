﻿using System;
namespace sambocapp_api.Models.AppAuthorize
{
	public class JWTTokenResponse
	{
        public string? Token
        {
            get;
            set;
        }
    }
}

