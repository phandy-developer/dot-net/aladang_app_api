﻿using System;
using System.ComponentModel.DataAnnotations;

namespace sambocapp_api.Models.AppAuthorize
{
	public class AuthenticateRequest
	{
        [Required]
        public string? Username { get; set; }

        [Required]
        public string? Password { get; set; }
    }
}

