﻿using System;
using Microsoft.CodeAnalysis;
using sambocapp_api.Models;
using sambocapp_api.Models.BO.Req;

namespace sambocapp_api.Interface
{
	public interface ILocation
	{
        public List<Locations> GetAll();
        public List<Locations> GetLocations(int page);
        public double Count();
        public double PageCount();

        public double Count(string status);
        public double PageCount(string status);
        public List<Locations> GetByStatus(string status, int page);
        public Locations GetById(int id);
        public Locations CreateNew(Locations req);
        public Locations Update(Locations req);

        
    }
}

