﻿using System;
using sambocapp_api.Models;

namespace sambocapp_api.Interface
{
	public interface ISetUpFee
	{
        public double Count();
        public double PageCount();

        public List<SetUpFee> GetAll();
        public List<SetUpFee> GetSetUpFees(int page);
        public SetUpFee GetSetUpFeeById(int id);
        public SetUpFee Update(SetUpFee obj);
        public SetUpFee CreateNew(SetUpFee req);
    }
}

