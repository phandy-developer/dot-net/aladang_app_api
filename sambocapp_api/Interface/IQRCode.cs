﻿using System;
using sambocapp_api.Models;

namespace sambocapp_api.Interface
{
	public interface IQRCode
	{
        public double Count();
        public double PageCount();

        public List<QRCode> GetAll();
        public List<QRCode> GetQRCodes(int page);
        public QRCode GetQRCodeById(int id);
        public QRCode Update(QRCode obj);
        public QRCode CreateNew(QRCode req);
    }
}

