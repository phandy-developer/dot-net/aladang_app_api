﻿using System;
using sambocapp_api.Models;

namespace sambocapp_api.Interface
{
	public interface IReportType
	{
        public double Count();
        public double PageCount();

        public List<ReportType> GetAll();
        public List<ReportType> GetReportTypes(int page);
        public ReportType GetReportTypeById(int id);
        public ReportType Update(ReportType obj);
        public ReportType CreateNew(ReportType req);
    }
}

