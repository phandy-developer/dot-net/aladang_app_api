﻿using System;
using sambocapp_api.Models;

namespace sambocapp_api.Interface
{
	public interface IPaymentMethod
	{
        public double Count();
        public double PageCount();

        public List<PaymentMethod> GetAll();
        public List<PaymentMethod> GetPaymentMethods(int page);
        public PaymentMethod GetPaymentMethodById(int id);
        public PaymentMethod Update(PaymentMethod obj);
        public PaymentMethod CreateNew(PaymentMethod req);
    }
}

