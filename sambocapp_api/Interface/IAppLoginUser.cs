﻿using System;
using sambocapp_api.Models.BO.Req;
using sambocapp_api.Models.BO.Res;

namespace sambocapp_api.Interface
{
	public interface IAppLoginUser
	{
        public AppLoginAuthorizeRes appCustomerLogin(AppUserLoginReq req);
        public AppLoginAuthorizeRes appShopLogin(AppUserLoginReq req);
    }
}

